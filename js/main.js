var cash=document.getElementById('cash')

function timer(){
    const d = new Date()
    const year = d.getFullYear() 
    const month = d.getMonth()
    const date = d.getDate()
    const day = d.getDay()
    document.getElementById("liveTime").innerHTML=`${days[day]} ${date} ${months[month]} ${year} ${format(new Date)}`
}
var startTime=0
var endTime=0
window.onload = function () {
    var isStart=false
    var clickCount=5
    var start=document.getElementById('start')
    var startBTN= document.getElementById('btnStart')
    var labelBtn=document.getElementById('btnLabel')
    var stop=document.getElementById('stopAt')
    var totalMinute=document.getElementById('minute')
    var totalPayment=document.getElementById('payment')
    
   startBTN.onclick=(e)=>{
    var currentTime=formatHourMinute(new Date);
    clickCount--
    console.log(clickCount)
    switch(clickCount){
        case 1:
            labelBtn.innerHTML=`Start`
            labelBtn.style.color='#fff'
            labelBtn.style.fontWeight='800'
            start.innerHTML=`0:00`
            stop.innerHTML=`0:00`
            startTime=new Date();
            clickCount=5
            break;
        case 2:
            labelBtn.innerHTML=`Start`
            labelBtn.style.color='#fff'
            labelBtn.style.fontWeight='800'
            start.innerHTML=`0:00`
            stop.innerHTML=`0:00`
            totalMinute.innerHTML=`0`
            totalPayment.innerHTML=`0`
            clickCount=5
            break;
        case 3:
            labelBtn.innerHTML=`Clear`
            labelBtn.style.color='red'
            labelBtn.style.fontWeight='800'
            stop.innerHTML=`${currentTime}`
            endTime=new Date();
            let totalTime=(endTime-startTime)
            totalMinute.innerHTML=`${(totalTime/60000).toFixed(0)}`
            totalPayment.innerHTML=`${calculateMoney((totalTime/60000).toFixed(0))}`
            console.log((totalTime/60000).toFixed(0),"Total")
            console.log("start At: ", startTime," "," Stop At:",endTime)
            console.log(calculateMoney((totalTime/60000).toFixed(0)))
            break;
        case 4:
            labelBtn.innerHTML=`Stop`
            start.innerHTML=`${currentTime}`
            labelBtn.style.color='white'
            labelBtn.style.fontWeight='800'
            startTime=new Date();
            break;      
    }
   }
};
function calculateMoney(minute){
    console.log(minute)
    if (minute>=65){
        return 2000
    }else if (minute>=31 || minute >=60){
        return 1500
    }else if (minute>=16 || minute >=30){
        return 1000
    }else if ( minute <=15 ||minute>=0){
        return 500
    } 
}
function format(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var second=date.getSeconds()
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; 
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var formattedTime = hours + ':' + minutes + ':' + second + ampm;
    return formattedTime;
  }
  function formatHourMinute(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; 
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var formattedTime = hours + ':' + minutes +' '+ ampm;
    return formattedTime;
  }
setInterval(timer, 1000);
const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
]
const days = [
  'Sun',
  'Mon',
  'Tue',
  'Wed',
  'Thu',
  'Fri',
  'Sat'
]